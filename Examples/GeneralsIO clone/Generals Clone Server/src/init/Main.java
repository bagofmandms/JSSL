package init;

import game.GameImplementation;
import game.ServerImplementation;
import bagofmandms.main.logging.Logger;

public class Main {
    Logger logger;
    ServerImplementation server;
    GameImplementation game;
    public Main(){
        logger = new Logger("resources");
        game = new GameImplementation(logger, server, 4);
        server = new ServerImplementation(logger, 4, 8080, "helloworld", game);
        server.open();
    }
}
