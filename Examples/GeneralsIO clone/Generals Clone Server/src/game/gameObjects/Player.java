package game.gameObjects;

public class Player {
    int port;
    int team;
    boolean active = false;
    public Player(){

    }
    public Player(int port, int team){
        this.port = port;
        this.team = team;
        active = true;
    }
    public boolean isPlayer(int port){
        if(port == this.port){
            return true;
        }
        return false;
    }
    public int getTeam(){
        return team;
    }
    public boolean isActive(){
        return active;
    }
    public int getPort(){
        return port;
    }
}
