package game.gameObjects;

import game.GameImplementation;

import java.util.Random;

public class Tile {
    int type;
    /* 0 = empty
     * 1 = mountain
     * 2 = monastery
     * 3 = king
     */
    GameImplementation parent;
    int team;
    int value;
    public Tile(GameImplementation parent){
        this.parent = parent;
        Random random = new Random();
        int selector = random.nextInt(99);
        if(selector < 50){
            type = 0;
            value = 0;
            team = 0;
        }else if(selector >= 60){
            type = 1;
            value = 0;
            team = 0;
        }else{
            type = 2;
            value = 40 + random.nextInt(9);
            team = -1;
        }
    }
    public void tick(){
        if(team != 0 && team != -1){
            value++;
        }
    }
    public boolean isActive(){
        if(type >= 2 && team != -1) {
            return true;
        }
        return false;
    }
    public void invadedBy(Tile invader){
        if(value > invader.value - 1){
            value = value - (invader.value - 1);
        }else if(value < invader.value - 1){
            if(type == 3){
                parent.kill(team);
            }
            value = (invader.value - 1) - value;
            team = invader.team;
        } else if(value == invader.value - 1){
            value = 0;
            team = -1;
        }
    }
    public String getData(){
        return type + ":" + team + ":" + value;
    }
}
