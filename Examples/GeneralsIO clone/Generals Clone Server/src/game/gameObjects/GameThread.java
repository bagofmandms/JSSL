package game.gameObjects;

import game.GameImplementation;

import java.util.ArrayList;

public class GameThread extends Thread {
    Tile[][] map;
    ArrayList<Event>[] eventQueues;
    boolean running = false;
    int cycle = 0;
    GameImplementation parent;
    int xSize = 10;
    int ySize = 10;
    public GameThread(GameImplementation parent, int size, int xSize, int ySize){
        this.parent = parent;
        this.xSize = xSize;
        this.ySize = ySize;
        eventQueues = new ArrayList[size];
        for(ArrayList<Event> queue : eventQueues){
            queue = new ArrayList<>();
        }
        map = new Tile[xSize][];
        for(int x = 0; x < xSize; x++){
            map[x] = new Tile[ySize];
            for(int y = 0; y < ySize; y++){
                map[x][y] = new Tile(parent);
                System.out.print(" | " + map[x][y].type + " : " + map[x][y].value);
            }
            System.out.println("");
        }
    }

    public void addEvent(Event event, int team){
        eventQueues[team].add(event);
    }

    @Override
    public void run() {
        try{
            while (running){
                if(cycle % 20 == 0) {
                    for (int x = 0; x < 50; x++) {
                        for (int y = 0; y < 50; y++) {
                            map[x][y].tick();
                        }
                    }
                }
                for(int x = 0; x < 50; x++){
                    for(int y = 0; y < 50; y++){
                        if(map[x][y].isActive()) {
                            map[x][y].value++;
                        }
                    }
                }
                runQueue();
                sleep(500);
            }
        }catch(Exception e){
            parent.passLog(e.toString());
        }
    }
    public void runQueue(){
        for(int i = 0;  i < eventQueues.length; i++){
            if(parent.playerAlive(i) && eventQueues[i].size() != 0){
                runEvent(eventQueues[i].get(i));
                eventQueues[i].remove(0);
            }
        }
    }
    public void runEvent(Event event){
        int xo = 0;
        int yo = 0;
        switch(event.d){
            case 0:
                yo = -1;
                break;
            case 1:
                xo = 1;
                break;
            case 2:
                yo = 1;
                break;
            case 3:
                xo = -1;
                break;

        }
        if(map[event.x + xo][event.y + yo].type != 1){
            map[event.x + xo][event.y + yo].invadedBy(map[event.x][event.y]);
            map[event.x][event.y].value = 1;
        }
    }
    public String getMap(){
        String mapData = "";
        for(int x = 0; x < xSize - 1; x++){
            for(int y = 0; y < ySize - 1; y++){
                mapData += map[x][y].getData() + "#";
            }
            mapData += map[x][ySize-1].getData() + "*";
        }
        mapData += map[xSize-1][ySize-1];
        return mapData;
    }
}