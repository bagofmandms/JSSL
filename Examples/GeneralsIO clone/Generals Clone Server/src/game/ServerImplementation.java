package game;

import bagofmandms.main.logging.Logger;
import bagofmandms.main.networking.Client;
import bagofmandms.main.networking.ClientController;
import bagofmandms.main.networking.GameController;
import bagofmandms.main.networking.Server;

public class ServerImplementation extends Server {
    public ServerImplementation(Logger logger, int size, int regPort, String password, GameController game){
        super(logger, size, regPort, password, game);
    }
    @Override
    protected ClientController getController(Logger logger, int port, Client client) {
        return new ClientInterface(logger, port, client);
    }
}
