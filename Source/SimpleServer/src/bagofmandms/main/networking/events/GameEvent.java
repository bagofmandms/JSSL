package bagofmandms.main.networking.events;

public class GameEvent {
    /**
     * Used to pass events from the client listener to the game controller.
     */
    public String data;
    public String caller;
    public GameEvent(String data, String caller){
        this.data = data;
        this.caller = caller;
    }
}