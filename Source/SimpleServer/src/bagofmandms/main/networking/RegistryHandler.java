package bagofmandms.main.networking;

import bagofmandms.main.logging.Loggable;
import bagofmandms.main.logging.Logger;

public class RegistryHandler extends Loggable {
    /**
     * Object which is loggable used to manage the registry thread.
     * Should be left alone.
     */
    protected Server parent;
    protected int port;
    protected String password;
    private RegistryThread thread;
    public RegistryHandler(Logger logger){
        super("Registry Handler", logger);
        init();
    }
    public void open(){
        log("Registry opened on port " + port);
        thread.setRunning();
    }
    public void close(){
        log("Registry closed on port " + port);
        thread.stopRunning();
    }
    public void setPassword(String password){
        this.password = password;
        log("password set to " + password);
    }
    public void setPort(int port) {
        this.port = port;
        log("port set to " + port);
    }
    private void init(){
        thread = new RegistryThread(this);
    }
    protected void passLog(String data){
        log(data);
    }
    protected String callGetClientPort(){
        return parent.getClientPort();
    }
}
