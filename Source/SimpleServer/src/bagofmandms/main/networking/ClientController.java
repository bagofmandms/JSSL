package bagofmandms.main.networking;

import bagofmandms.main.logging.Loggable;
import bagofmandms.main.logging.Logger;
import bagofmandms.main.networking.events.GameEvent;

public abstract class ClientController extends Loggable {
    /**
     * Externally defined object which handles message received events and can interact with the game controller.
     */
    private Client parent;
    public ClientController(Logger logger, int port, Client parent){
        super("Client " + port, logger);
        this.parent = parent;
    }

    /**
     * Allows logging to client logID from related objects.
     * @param data
     * Message logged.
     */
    protected void passLog(String data){
        log(data);
    }

    /**
     * Called when data is received over port.
     * @param message
     * data received.
     */
    public abstract void MessageReceivedEvent(String message);

    /**
     * Called when client is disconnected.
     * Should be used to store data, etc.
     */
    public abstract void Restart();

    /**
     * Sends data over port.
     * @param data
     * data sent over port.
     */
    public void send(String data){
        parent.send(data);
    }

    /**
     * sends an event to the game manager.
     * @param data
     * data associated with the event in the form of a string.
     */
    public void sendGameEvent(String data){
        parent.passGameEvent(new GameEvent(data, Integer.toString(parent.getPort())));
    }
}
