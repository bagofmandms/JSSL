package bagofmandms.main.logging;

import java.io.File;
import java.io.FileOutputStream;
import java.time.Instant;

public class Logger {
    /**
     * Generates and manages log file. Must be initialized and stored externally then passed into objects which require the logger.
     */
    private FileOutputStream writer;
    private String outLocation;
    public Logger(String outLocation){
        init(outLocation);
    }

    /**
     * Sets file to generate log in.
     * @param outLocation
     * path to the desired folder for the file (Not the txt file!).
     */
    public void setOutLocation(String outLocation){
        init(outLocation);
    }
    private void generateWriter(){
        try {
            writer = new FileOutputStream(outLocation + "/log.txt", true);
        }catch(Exception e){
            System.out.println("Logger error! " + e);
        }
    }

    /**
     * Writes a message to the file.
     * @param caller
     * Defines the tag for the log entry.
     * @param string
     * Defines the message for the log entry.
     */
    public void write(String caller, String string){
        try {
            writer.write(("[" + caller + "]" + string + "\n").getBytes());
        }catch(Exception e){
            System.out.println("Logger failed! " + e);
        }
    }
    private void init(String outLocation){
        this.outLocation = outLocation;
        try{
            File log = new File(this.outLocation + "/log.txt");
            log.createNewFile();
            generateWriter();
            write("Logger", "Initialized at " + Instant.now());
        }catch(Exception e){
            System.out.println("Logger error! " + e);
        }
    }
}
