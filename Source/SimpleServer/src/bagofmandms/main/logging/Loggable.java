package bagofmandms.main.logging;

public class Loggable {
    /**
     * An object which can write to the log file.
     * Use super(ID, log reference) to initialize a loggable object.
     * log() can be used to write a string to the log file.
     */
    private String logID;
    private Logger logger;
    public Loggable(String logID, Logger logger){
        this.logID = logID;
        this.logger = logger;
    }
    protected void log(String data){
        logger.write(logID, data);
    }
}
